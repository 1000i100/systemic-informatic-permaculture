Présentation en ligne :
- https://huit.re/perma-sys-info
- https://1000i100.frama.io/systemic-informatic-permaculture/

## Geconomicus
### But :
Répondre au mieux à ses besoins en produisant un maximum de valeur (cartes)

### Mise en place :
5/4*NbrJoueur Carré de cartes (pour chaque couleur)
autant de billet que de joueur (ou plus) pour chaque couleur

### Échanges :
- 1 carte basse s'échange pour 1 pièce haute (ou 2 moyenne ou 4 basse).
- 1 carte moyenne s'échange pour 2 pièces hautes (ou équivalent)
- 1 carte haute s'échange pour 4 pièces hautes (ou équivalent)

### Monnaie dette :
tout le monde part avec 4 cartes basses et 2 billets via emprunt (donc doit rembourser 1 à chaque tour ou 3 pour se libérer de l'emprunt)

Le banquier ne prête qu'à quelqu'un qui peu rembourser (et réclame le remboursement total si la personne risque de ne plus pouvoir payer les intérêts plus tard).

Est solvable quelqu'un qui à suffisamment de cartes pour pouvoir saisir en plus de la monnaie empruntée, les bénéfices + les frais.

Si quelqu'un n'a pas de quoi payer en billet, il doit rembourser en nature + frais de dossier d'une carte, voir 2 si le banquier le décide (si la personne rembourse 100% en nature par exemple)

Le banquier note ses remboursements d'intérêt (comme bénéfices) associés à au nombre de révolutions technologique pour la compta finale.



# monnaie et économie
Géconomicus (atelier découverte pratique)

histoire/panorama monnaies
- jubilé
-

- rôle / pouvoir des banques -> création monétaire & [choix des projets bankable plutôt qu'utile/éthique](https://tube.conferences-gesticulees.net/videos/watch/54c68e02-60fc-4b9e-9275-6bdc347760e3?start=33m50s)
- [macro-économie sans monnaie, la norme, prisme déformant dramatique](https://tube.aquilenet.fr/videos/watch/dde423e4-5280-47da-a4bf-4b9ad01d4bdb?start=26s).

La propriété c'est le vol ->
[End:Civ](https://tube.aquilenet.fr/videos/watch/0ef5d380-d7e6-466e-8c93-526f281f8298)
[Tous propriétaires](https://tube.conferences-gesticulees.net/videos/watch/7dff9eaf-4349-425a-b8d4-3892b74bf3d7)

Chiffres/sources :
- energie internet : [3% de l'électricité mondiale](https://theshiftproject.org/wp-content/uploads/2018/10/R%C3%A9sum%C3%A9-aux-d%C3%A9cideurs_Pour-une-sobri%C3%A9t%C3%A9-num%C3%A9rique_Rapport_The-Shift-Project.pdf) en [augmentation expodentielle](https://theshiftproject.org/article/shift-project-vraiment-surestime-empreinte-carbone-video-analyse/). [Delta sobriété energétique selon les site x25](https://www.techniques-ingenieur.fr/actualite/articles/la-consommation-energetique-des-sites-internet-tres-inegale-etude-1545/)
  [Si Internet était un pays, il serait le 3ème plus gros consommateur d’électricité au monde avec 1500 TWH par an, derrière la Chine et les Etats-Unis. Au total, le numérique consomme 10 à 15 % de l’électricité mondiale, soit l’équivalent de 100 réacteurs nucléaires. Et cette consommation double tous les 4 ans !](https://www.fournisseur-energie.com/internet-plus-gros-pollueur-de-planete/)
  https://cayla.de/wp-content/uploads/2019/01/energie_internet-1-1024x1024.gif
- [monnaie économie réelle / marché financier](https://blogs.alternatives-economiques.fr/gadrey/2014/09/13/la-finance-pese-t-elle-100-fois-plus-que-l-economie-reelle-10-fois-plus-bien-moins)


# systemic-informatic-permaculture

## Systémique

1. [Théorie des jeux : Confiance, collaboration, mondialisation](https://ayowel.github.io/trust/) -> interactions répétées != mondialisation & triche, gagnant-gagnant != somme nulle. Cognatus (À court terme, le jeu définit les joueurs. Mais à long terme, c'est nous les joueurs qui définissons le jeu).
2. [Effets émergeant](https://tube.aquilenet.fr/videos/watch/e081ec39-5758-4f98-a483-7445e573f487) -> toucher une statue != marteau joconde, pas de grand méchant -> bienveillance != bientraitance -> intension != résultat systémique -> hiérarchie -> fétichisation du contrôle -> panoptique & peur & oppression intériorisée != confiance & implication
3. ccl : Sans volonté de nuire, de nos choix/organisation/culture découlent des dysfonctionnements.
   Plutôt que chercher à endiguer les symptômes, Prenons le recul nécessaire pour voir le système dans son ensemble et ajustons nos fonctionnements/comportements pour faire évoluer le système.


Ressources bonus en vrac :
[Evolutions croyances : sagesse et folie des foules](https://ncase.me/crowds/fr.html)
[Racisme, conformisme, diversité](http://ncase.me/polygons-fr/)
[Votes, mode de scruttin](https://xdeadc0de.github.io/ballot-fr/)
[Covid-19](https://catheu.tech/covid-19/)

## Informatique

1. [Dystopie en cours d'installation](https://medium.com/predict/a-21st-century-panopticon-called-facebook-a312949c08fb), merci de bien vouloir patienter. ([Benjamen Bayard sur Thinkerview](https://www.youtube.com/watch?v=VBsLSfPs2PE))
   - obsession du contrôle : [capitalisme de surveillance](https://www.monde-diplomatique.fr/2019/01/ZUBOFF/59443) -> [concentration du pouvoir](https://ressources.1forma-tic.fr/presentation/secu-info/diapo/#/36)
   - economie -> rareté (avant, rareté des marchandise, puis de l'information. Maintenant qu'il y a abondance, [rareté de l'accès au cerveau](https://usbeketrica.com/article/sur-son-lit-de-mort-personne-ne-se-dit-j-aurais-aime-passer-plus-de-temps-sur-facebook) -> [économie de l'attention](https://vimeo.com/316653200))
   - le piège de la gratuité -> votre attention, vos contribution, leur profit (attention -> publicitaire/leader d'opignon ; contribution -> plateforme)
2. [mathwashing](https://www.mathwashing.com/) tout est politique, sortir de l'impuissance, reprendre le pouvoir -> vertical != horizontal, code de loi == code informatique, /!\ illetrisme/fracture numérique.
3. ATELIER
3. [logiciel libre](https://ressources.1forma-tic.fr/presentation/utopie-libre/#/), réseau/communauté libre (interopérabilité, fediverse, p2p), infrastructure libre (maillée, associative), hardware libre, monnaie libre, savoir libre, culture libre (stop propriété intellectuelle)... (et entreprises libérées)
4. l'internet-utopique de demain : concrètement, aproche pas à pas ou révolution de paradigme ? La liberté ne s'impose pas, la conscience déjà un peu plus (capital culturel)
5. et dans 1000 ans ? [comment faire de la hi-tech 100% renouvellable](https://www.youtube.com/watch?v=S0OFfuUOX5U), pas à 99% à 100% (hors apport d'énergie solaire directe ou indirecte) -> [biologie syntéthique](http://www.genevieve-fioraso.com/wp-content/uploads/2011/11/coursScPoParis_biologiesynth%C3%A8se.pdf) ? [Attention aux dangers notre culture manque de sagesse](https://www.zdnet.com/article/what-utopia-can-technology-deliver/) (solutionnisme technologique, transhumanisme à l'ère du capitalisme).
6. ccl : l'informatique, un outil politique puissant, d'oppression comme de libération, s'en protéger et s'en saisir individuellement et collectivement.

### Atelier :
Incarnez les acteurs du numérique et discutez pour comprendre ce qui se joue entre vous, et si vous voulez changer ça.
Gardez en tête la différence entre vos objectifs en tant qu'acteurs d'un système et les effets émergant du système que vous engendrez.

Rôles :
- Usager
- Publicitaire (employé, patron, client privé ou politique)
- Plateforme (codeur, modérateur, patron)

Objectifs :
- identifier les enjeux de chacun
- identifier les liens de pouvoir, qui décide de quoi et l'impose à qui.
- Si vous donniez tout pouvoir à l'usager, que changerez-t-il ?
- Concrètement, demain, dans votre vie, que pouvez-vous faire pour reprendre du pouvoir en tant qu'usager ? (Ou pour en donner plus à l'usager si vous êtes à la place d'un autre rôle)


------------
https://en.wikipedia.org/wiki/File:Converging_technologies.png#/media/File:Converging_technologies.svg
