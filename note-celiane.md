Attention ça va être dense, si vous décrochez, interrompez-moi, c’est mieux pour tout le monde.


## Systémique

Qu’est ce qu’un système ?
Des entités (personnes, rôles, équipes…), des liens / interactions, un titre.
Fractalisation d’un système : groupe vs. individus

### Théorie des jeux :
Celui qui triche est avantagé mais si tout le monde triche il n’y a aucun gain. Qu’est ce qui est le plus rentable ? Jouer perso ou jouer coopératif ? A quel point ça change en fonction du comportement de l’autre ? Profils de joueurs, coopératifs, imitation, basé sur le style de jeu de l’autre, essais aléatoires… Qui est le gagnant selon le nombre d’interaction ? Selon la proportion de tricheurs dans le lot, qui sont les gagnants ? Mixité de profils
Qu’est ce qui évolue chez les joueurs quand les règles changent ?

Plus la société est faite d’interactions répétées entre peu de personnes, plus ça encre la coopération et va créer des relations de confiance.
Plus la société fonctionne avec des interactions éparses avec énormément de personnes différentes, plus cela va encourager la méfiance et des comportements individualistes.

Si on considère que dans un jeu ou une interaction, le total est toujours nul, ça ne permet pas de penser la collaboration (gagner veut dire enlever quelque chose à l’autre). Penser en dehors du prisme de la somme nulle permet de chercher des solutions où tout le monde est gagnant.

### Conatus : à court terme, le jeu définit les joueurs.
Interinfluence, la façon de voir le monde façonne les interactions de ce monde, et donc les règles de ce monde. Les règles de ce monde façonne la façon de voir le monde et d’interagir dedans. Mais c’est les interactions de ce monde qui font évoluer les règles de ce monde.

### Effets émergeant
Dans un système, il y a plusieurs acteurs, et les produits de ce systèmes peuvent n’être souhaités par personne dans ce système, c’est un comportement émergeant.

Exemple : entreprise ou production de tomates
Il n’y a pas de grand méchant, personne n’est à la tête du système pour instaurer des règles de merde. Les entités du système ne voient souvent pas les effets de leurs comportements, il faut une capacité à dézoomer. Que des gens qui font de leur mieux dans le système ?

[Que faire face à ça ? Une intention ne produit pas un résultat systémique.
Trragedy of commons : violence individuelle dérisoire à un niveau systémique ; ou geste anodin individuellement très violent à un niveau systémique. Le marteau sur la Joconde ou caresser une statue. ]

Exemple de la société : plus on est dans une grosse structure (multinationale ou Etat administratif), plus on a un grand nombre de personne. Envie de contrôler pour éviter les comportements déviants / non-coopératifs. Avec une hiérarchie, le contrôle va toujours dans la même direction, et plus le degré de contrôle est important en fonction du niveau d’échelons. Fétichisation du contrôle.
Dès lors que l’objectif c’est la croissance, le comportement émergeant est un contrôle et une mise sous pression pour lisser tous les comportements individuels déviants de cette direction.

Conséquence : l’autocontrôle de ses propres comportements. Le système devient un panoptique,

Ouverture : faire confiance aux individus pour servir une raison d’être et faire des choix orientés vers cette raison d’être. Prendre conscience de ce qui se joue au niveau commun plutôt qu’individuel. Envie de contribuer. Passer d’un système où les acteurs agissent sous la contrainte à un système où les acteurs servent un but commun et sont libres dans leur façon de servir ce but commun.


## Informatique

### En quoi l’informatique c’est tout pourri dans nos vies ? (Version soft : En quoi l’informatique nous prive de nos libertés ?)
Facebook : effet de la quantité de personnes pour en attirer d’autres, diversité de contenus pour cibler précisément les besoins/préférences des usagers afin que les publicitaires aient un plus grand impact. Monétisation du bénévolat/loisir/divertissement : l’usager consulte sur son temps libre mais rien n’échappe à la sphère marchande. Plus on passe de temps sur la plateforme, plus les publicitaires ont accès à nous. L’évolution de l’économie de marché : rareté de marchandise, puis une rareté d’information, puis rareté d’accès à notre attention (cerveau disponible).
Aspects : marchand, propagande, surveillance, prédiction des comportements. Capitalisme de surveillance : capter de la valeur par la surveillance, et c’est nous qui opérons cette surveillance puisque nous fournissons de quoi nous surveiller.

### Mathwashing
Prendre conscience de tout ça pour agir dessus ?
Mathwashing : Arguer la déresponsabilisation individuelle par soumission à l’informatique et à l’algorythme.
Mettre la technologie à notre service et lui redonner une dimension politique.

#### Atelier : chacun a un rôle et doit discuter avec les autres membres de son trio. (Usager, publicitaire, plateforme)
1. Comprendre les enjeux de chacun
2. Identifier les liens de pouvoir (qui décide quoi et l’impose à qui?)
3.  Si on donne tout pouvoir à l’usager, que veut-il changer ?
4. Sortez de votre personnage, vous dans votre vie, vous pouvez faire quoi en tant qu’usager pour faire changer les choses ?

 
### Comment transformer l'outil informatique pour servir nos causes ?
Qu’est ce que ça apporte en plus d’utiliser l’informatique au service de nos idéaux ?

Approches rapides : Différentes façons concrètes de faire du libre et de l’horizontal avec l’informatique.
Approche pas à pas : la liberté ne s’impose pas, mais la prise de conscience réduit le retour en arrière (fait évoluer le seuil de non-retour) et favorise l’adoption volontaire. Faire comprendre l’impact des usages pour laisser la personne libre de choisir à partir de ces nouvelles consciences.

### Conclusion : 
L’informatique est un outil puissant d’oppression comme d’émancipation.

### Ouverture : 
La survie de l’outil numérique dans le temps : Est-ce que ça fait sens de promouvoir ça ? Risque de rendre indispensable un outil qui ne peut pas durer ?




