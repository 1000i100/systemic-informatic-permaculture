var TitleArea;

TitleArea = function() {
  var includeModuleSpecificCss;
  includeModuleSpecificCss = function() {
    var css;
    css = document.createElement('link');
    css.setAttribute('rel', 'stylesheet');
    css.setAttribute('href', 'plugin/titleArea/titleArea_auto.css');
    return document.head.appendChild(css);
  };
  return includeModuleSpecificCss();
};
