var DynamicMenuBar;

DynamicMenuBar = function(data) {
  var DataMenuOnlyAnalyser, HtmlAnalyser, addSectionTagIndex, addTreeIndexForNoTitleSlide, buildMenuTree, convertTreeToHtml, extend, getNextCategorieSlideNumber, getPresentCategorieSlideNumber, htmlMenuTree, includeLinkCss, insertHtmlTree, menuTree, resetCurrentEtape, resetMenuPosition, showCurrentEtape, tagMenuPast, tagMenuPresent, updateMenuProgress, updateOnEvents;
  extend = function(a, b) {
    var i, results;
    results = [];
    for (i in b) {
      results.push(a[i] = b[i]);
    }
    return results;
  };
  includeLinkCss = function() {
    var menuBarCss;
    menuBarCss = document.createElement('link');
    menuBarCss.setAttribute('rel', 'stylesheet');
    menuBarCss.setAttribute('href', 'plugin/dynamicMenuBar/dynamicMenuBar_auto.css');
    return document.head.appendChild(menuBarCss);
  };
  insertHtmlTree = function(tree) {
    var navBar;
    navBar = document.createElement('nav');
    navBar.innerHTML = tree;
    return document.body.appendChild(navBar);
  };
  convertTreeToHtml = function(data) {
    var htmlMenuTree, recursiveHtmlBuilder;
    htmlMenuTree = '<ul>';
    recursiveHtmlBuilder = function(data) {
      var i, results;
      i = 0;
      results = [];
      while (i < data.length) {
        htmlMenuTree = htmlMenuTree + '<li treeIndex="' + data[i].treeIndex + '"><a href="#/' + data[i].slideIndex + '">' + data[i].title + '</a>';
        if (data[i].children.length > 0) {
          htmlMenuTree = htmlMenuTree + '<ul>';
          recursiveHtmlBuilder(data[i].children);
          htmlMenuTree = htmlMenuTree + '</ul>';
        }
        htmlMenuTree = htmlMenuTree + '</li>';
        results.push(i++);
      }
      return results;
    };
    recursiveHtmlBuilder(data);
    htmlMenuTree = htmlMenuTree + '</ul>';
    return htmlMenuTree;
  };
  buildMenuTree = function(analyser) {
    var index, initialLevel, iterable, recursiveBuilder, structuredIndex;
    iterable = analyser.getIterable(document);
    index = 0;
    structuredIndex = [];
    initialLevel = analyser.analyse(iterable[index]).level;
    recursiveBuilder = function(level) {
      var children, menuData, menuNextLevel, tree;
      tree = [];
      while (index < iterable.length) {
        children = [];
        if (!structuredIndex[level - 1]) {
          structuredIndex[level - 1] = 0;
        }
        structuredIndex[level - 1]++;
        iterable[index].parentNode.setAttribute('treeIndex', structuredIndex.join('.'));
        menuData = analyser.analyse(iterable[index]);
        menuNextLevel = 0;
        if (index + 1 < iterable.length) {
          menuNextLevel = analyser.analyse(iterable[index + 1]).level;
        }
        if (menuNextLevel > level) {
          index++;
          children = recursiveBuilder(menuNextLevel);
        }
        tree.push({
          'title': menuData['title'],
          'treeIndex': menuData['treeIndex'],
          'slideIndex': menuData['slideIndex'],
          'children': children
        });
        if (index + 1 < iterable.length) {
          menuNextLevel = analyser.analyse(iterable[index + 1]).level;
        }
        if (menuNextLevel < level) {
          structuredIndex.pop();
          break;
        }
        index++;
      }
      return tree;
    };
    return recursiveBuilder(initialLevel);
  };
  HtmlAnalyser = function() {
    this.getIterable = function(data) {
      return data.querySelectorAll('h1,h2,h3,h4,h5,h6');
    };
    this.analyse = function(data) {
      return {
        'level': data.tagName.substring(1),
        'title': data.getAttribute('menu') || data.innerHTML,
        'treeIndex': data.parentNode.getAttribute('treeIndex'),
        'slideIndex': data.parentNode.getAttribute('slideIndex')
      };
    };
    return this;
  };
  addTreeIndexForNoTitleSlide = function() {
    var i, results, slide, slides, treeIndex;
    slides = document.querySelectorAll('[slideindex]');
    treeIndex = 0;
    i = 0;
    results = [];
    while (i < slides.length) {
      slide = slides[i];
      if (slide.getAttribute('treeIndex')) {
        treeIndex = slide.getAttribute('treeIndex');
      } else {
        slide.setAttribute('treeIndex', treeIndex);
      }
      results.push(i++);
    }
    return results;
  };
  DataMenuOnlyAnalyser = function() {};
  addSectionTagIndex = function() {
    var hSectionList, i, j, results, vSectionList;
    hSectionList = document.querySelectorAll('.slides>section');
    i = 0;
    results = [];
    while (i < hSectionList.length) {
      hSectionList[i].setAttribute('slideIndex', i);
      vSectionList = hSectionList[i].querySelectorAll('section');
      j = 0;
      while (j < vSectionList.length) {
        vSectionList[j].setAttribute('slideIndex', i + '/' + j);
        j++;
      }
      results.push(i++);
    }
    return results;
  };
  resetMenuPosition = function() {
    var i, presentLi, resetList, results;
    resetList = document.querySelectorAll('nav li.present, nav li.past');
    i = 0;
    results = [];
    while (i < resetList.length) {
      presentLi = resetList[i];
      presentLi.className = '';
      results.push(i++);
    }
    return results;
  };
  tagMenuPresent = function(currentSlide) {
    var currentIndexParts, results, selected, selector;
    currentIndexParts = currentSlide.getAttribute('treeIndex').split('.');
    results = [];
    while (currentIndexParts.length) {
      selector = 'li[treeIndex="' + currentIndexParts.join('.') + '"]';
      selected = document.querySelector(selector);
      if (selected) {
        selected.className = 'present';
      }
      results.push(currentIndexParts.pop());
    }
    return results;
  };
  tagMenuPast = function() {
    var e, i, presentsParts, results, stepBefore, tagableParts;
    presentsParts = document.querySelectorAll('nav .present');
    stepBefore = function(eList) {
      var elem, key, newList;
      newList = [];
      for (key in eList) {
        elem = eList[key];
        if (elem.previousElementSibling && elem.previousElementSibling.tagName === 'LI') {
          newList.push(elem.previousElementSibling);
        }
      }
      return newList;
    };
    tagableParts = stepBefore(presentsParts);
    results = [];
    while (tagableParts.length) {
      for (i in tagableParts) {
        e = tagableParts[i];
        e.className = 'past';
      }
      results.push(tagableParts = stepBefore(tagableParts));
    }
    return results;
  };
  resetCurrentEtape = function() {
    var i, presentLi, resetList, results;
    resetList = document.querySelectorAll('a[class^="etape"]');
    i = 0;
    results = [];
    while (i < resetList.length) {
      presentLi = resetList[i];
      presentLi.className = '';
      results.push(i++);
    }
    return results;
  };
  getNextCategorieSlideNumber = function(elem) {
    var nextElement, nextTreeIndex, slides, treeFragment;
    if (elem.nextElementSibling) {
      return elem.nextElementSibling.firstElementChild.getAttribute('href').split('/')[1];
    } else {
      treeFragment = elem.getAttribute('treeIndex').split('.');
      while (treeFragment.length) {
        treeFragment.pop();
        treeFragment[treeFragment.length - 1] = parseInt(treeFragment[treeFragment.length - 1]) + 1;
        nextTreeIndex = treeFragment.join('.');
        nextElement = document.querySelector('li[treeIndex="' + nextTreeIndex + '"]');
        if (nextElement) {
          return getPresentCategorieSlideNumber(nextElement);
        }
      }
      slides = document.querySelectorAll('[slideIndex]');
      return slides[slides.length - 1].getAttribute('slideIndex');
    }
  };
  getPresentCategorieSlideNumber = function(elem) {
    return elem.firstElementChild.getAttribute('href').split('/')[1];
  };
  showCurrentEtape = function(currentSlide) {
    var calculateClassEtape, elem, k, len, presentList, results;
    calculateClassEtape = function(currentSlide, target) {
      var current, currentSlideNumber, nextCategorieSlideNumber, presentCategorieSlideNumber, total;
      nextCategorieSlideNumber = getNextCategorieSlideNumber(target);
      presentCategorieSlideNumber = getPresentCategorieSlideNumber(target);
      currentSlideNumber = currentSlide.getAttribute('slideindex');
      total = nextCategorieSlideNumber - presentCategorieSlideNumber;
      current = currentSlideNumber - presentCategorieSlideNumber;
      return 'etape' + current + 'sur' + total;
    };
    presentList = document.querySelectorAll('nav li.present');
    results = [];
    for (k = 0, len = presentList.length; k < len; k++) {
      elem = presentList[k];
      console.log(elem.firstElementChild.innerText, elem.firstElementChild.getAttribute('href').split('/')[1]);
      elem.firstElementChild.classList.add(calculateClassEtape(currentSlide, elem));
      results.push(console.log(calculateClassEtape(currentSlide, elem)));
    }
    return results;
  };
  updateMenuProgress = function(eventData) {
    var currentSlide;
    currentSlide = document.querySelector('.slides>section.present[treeIndex], section.stack.present section.present[treeIndex]');
    if (currentSlide) {
      resetMenuPosition();
      tagMenuPresent(currentSlide);
      tagMenuPast();
      resetCurrentEtape();
      return showCurrentEtape(currentSlide);
    }
  };
  updateOnEvents = function(eventTabList) {
    var i, results, revealEventTransmitter;
    revealEventTransmitter = document.querySelector('.reveal');
    results = [];
    for (i in eventTabList) {
      results.push(revealEventTransmitter.addEventListener(eventTabList[i], updateMenuProgress));
    }
    return results;
  };
  addSectionTagIndex();
  includeLinkCss();
  menuTree = buildMenuTree(new HtmlAnalyser);
  addTreeIndexForNoTitleSlide();
  htmlMenuTree = convertTreeToHtml(menuTree);
  insertHtmlTree(htmlMenuTree);
  return updateOnEvents(['cmhChanged']);
};
