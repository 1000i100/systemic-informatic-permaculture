# Status bar

This plugin add a themable status bar with some targets (default: left, center, right) and dynamic keyword like {{page}}, {{subPage}} and {{totalPage}}.

## How to use it

- add the status-bar folder to your project plugin folder.
- add this in your index.html file, dependency section near the end of your file.

```
{ src: 'plugin/status-bar/status-bar.js', async: true, callback: function() {
	statusBar.init({
		left:'CC-BY-SA',
		center:'~ page {{page}}{{subPage}}/{{totalPage}} ~',
		right:'Millicent Billette © <img alt="1twitif" src=""/>'
	}); }
},
```
- edit the left, center and right param to display what you want in the status bar.

**Note:** if you want others targets than left, center and right, you will need to write extra css.
