(function() {
  var LightenDarkenColor, rgbToHex, updateBackground, updateOnEvents;
  rgbToHex = function(r, g, b) {
    return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
  };
  LightenDarkenColor = function(col, amt) {
    var b, g, num, r, usePound;
    usePound = false;
    if (col[0] === '#') {
      col = col.slice(1);
      usePound = true;
    }
    num = parseInt(col, 16);
    r = (num >> 16) + amt;
    if (r > 255) {
      r = 255;
    } else if (r < 0) {
      r = 0;
    }
    b = (num >> 8 & 0x00FF) + amt;
    if (b > 255) {
      b = 255;
    } else if (b < 0) {
      b = 0;
    }
    g = (num & 0x0000FF) + amt;
    if (g > 255) {
      g = 255;
    } else if (g < 0) {
      g = 0;
    }
    return (usePound ? '#' : '') + (g | b << 8 | r << 16).toString(16);
  };
  updateBackground = function(eventData) {

    /*
    		récupérer la couleur de l'élément actuel du menu, le plus profond dans l'arboresence
    		appliquer cette couleur en fond de slide.
    
    		puis ajuster en ligten 20 et 40 pour en faire un dégradé radial plutot qu'un applat
     */
    var borderColor, centerColor, currentStep, stepColor, stepRgbColor;
    currentStep = document.querySelector('li.present a');
    stepRgbColor = window.getComputedStyle(currentStep).borderBottomColor.split('(')[1].split(')')[0].split(',');
    stepColor = rgbToHex(parseInt(stepRgbColor[0]), parseInt(stepRgbColor[1]), parseInt(stepRgbColor[2]));
    centerColor = "rgba(100%,100%,100%,0)";
    borderColor = 'rgba(' + stepRgbColor[0] + ',' + stepRgbColor[1] + ',' + stepRgbColor[2] + ',0.3)';
    return document.getElementsByTagName("body")[0].style.background = "radial-gradient(ellipse farthest-corner at center, " + centerColor + ", " + borderColor + ")";
  };
  updateOnEvents = function(eventTabList) {
    var i, results, revealEventTransmitter;
    revealEventTransmitter = document.querySelector('.reveal');
    results = [];
    for (i in eventTabList) {
      results.push(revealEventTransmitter.addEventListener(eventTabList[i], updateBackground));
    }
    return results;
  };
  return updateOnEvents(['cmhChanged']);
})();
